import { Layout } from "components/templates"
import HomePage from "container/HomePage"
import React from "react"

function Index() {
  return (
    <Layout className="container mx-auto">
      <HomePage />
    </Layout>
  )
}

/**
 * @see https://nextjs.org/docs/api-reference/data-fetching/getInitialProps
 */
// Index.getInitialProps = async (ctx: AppContext): Promise<Props> => {
//   const { store } = ctx
//   store.dispatch(
//     changePage({
//       id: Page.TOP.id,
//     })
//   )
//   return {}
// }

export default Index
