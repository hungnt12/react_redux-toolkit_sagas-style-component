import { Result } from "antd"
import Link from "next/link"
import React, { useEffect } from "react"
import { Layout } from "../components/templates"
import { Page } from "../constants"
import { usePage } from "../hooks"

/**
 * 404 Error page without getInitialProps(SSR)
 * @see https://nextjs.org/docs/advanced-features/custom-error-page#customizing-the-404-page
 * @see https://github.com/zeit/next.js/blob/master/errors/custom-error-no-custom-404.md
 */
function NotFoundError() {
  const { changePage } = usePage()

  useEffect(() => {
    if (changePage && Page.ERROR.id) {
      changePage(Page.ERROR.id)
    }
  }, [])

  return (
    <Layout>
      <Result
        status={404}
        title={404}
        subTitle="Sorry, you are not authorized to access this page."
        extra={
          <Link href={"/"}>
            <a className="btn btn-primary"> Quay lại trang chủ</a>
          </Link>
        }
      />
    </Layout>
  )
}

export default NotFoundError
