import React from "react"
import { AppContext } from "../components/AppContext"
import { Page } from "../constants"
import { changePage } from "../store/page"

type Props = {
  // passed from getInitialProps
  defaultInputNumber: number
}

function Redux() {
  return <></>
}

/**
 * @see https://nextjs.org/docs/api-reference/data-fetching/getInitialProps
 */
Redux.getInitialProps = async (ctx: AppContext): Promise<Props> => {
  const { store } = ctx
  store.dispatch(
    changePage({
      id: Page.REDUX.id,
    })
  )
  return {
    defaultInputNumber: 2,
  }
}

export default Redux
