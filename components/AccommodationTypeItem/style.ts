import styled from "styled-components";

const Item = styled.div`
  position: relative;
  width: 369.5px;
  padding-top: ${(250 / 369.5) * 100}%;
  margin: 0 auto;
  z-index: 1;
  .title {
    position: absolute;
    bottom: 18px;
    left: 23.2px;
    font-weight: bold;
    color: white;
  }
  .bg_gradient_botom {
    position: absolute;
    width: 100%;
    opacity: 80%;
    border-radius: 0px 0px 8px 8px;
    bottom: 0px;
    left: 0px;
    height: 92px;
    background-image: linear-gradient(to top, #005dbad4, #005dba03);
  }
`;

export default { Item };
