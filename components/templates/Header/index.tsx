import ImageUltils from "components/ImageUltils"
import styles from "components/templates/Header/style"
import Link from "next/link"
import React from "react"
function Header() {
  return (
    <styles.Container className={`container mx-auto`}>
      <div className="top">
        <Link href="/">
          <a id="logo">
            <ImageUltils src="/images/Logo.png" layout="fill" />
          </a>
        </Link>
        <nav id="nav-menu position-relative">
          <ul className="flex flex-wrap justify-end">
            <li>tiếng viêt</li>
            <li>Đăng nhập</li>
            <li>Đăng ký</li>
            <li>Liên Hệ</li>
            <li>Host</li>
          </ul>
        </nav>
      </div>
      <div className="bg-header">
        <ImageUltils src="/images/homePage/banner.svg" layout="fill" />
      </div>
    </styles.Container>
  )
}

export default Header
