import styled from "styled-components"

const Container = styled.header`
  position: relative;
  display: flex;
  padding-top: ${(273 / 1170) * 100}%;
  justify-content: space-between;
  .top {
    position: absolute;
    top: 0px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    nav {
      z-index: 1;
      ul {
        margin-top: 17px;
      }
    }
  }
  #logo {
    width: 127.88px;
    height: 45px;
    position: relative;
    z-index: 1;
    margin-top: 17px;
    display: inline-block;
  }
  .bg-header {
  }
`

export default { Container }
