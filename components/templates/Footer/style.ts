import styled from "styled-components"

const Container = styled.footer`
  background-color: #f2f2f2;
  height: 259px;
  padding-top: 70px;
  .coppy--right {
    border-top: solid 1px #e6e6e6;
    width: 100%;
    height: 64px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`

export default { Container }
