import style from "components/templates/Footer/style"
import React from "react"
function Index() {
  return (
    <style.Container className=" flex flex-col justify-between	items-center">
      <div className="w-max flex flex-wrap container mx-auto justify-around">
        <div>
          <img src="/images/footer/logo-footer.png" />
        </div>
        <ul>
          <li> CÔNG TY CỔ PHẦN GIẢI TRÍ BESTAY</li>
          <li> Trụ sở: Vinhomes Greenbay, Mễ Trì, Hà Nội</li>
          <li> Email: info@bestay.vn</li>
          <li> Hotline: (+84) 862 004 598</li>
        </ul>
        <ul>
          <li> TRUNG TÂM HỖ TRỢ</li>
          <li> Câu hỏi thường gặp</li>
          <li> Chính sách bảo mật</li>
          <li> Điều khoản sử dụng</li>
        </ul>
        <div>
          <h3>CỘNG ĐỒNG</h3>
          <div className="flex">
            <img src="/images/footer/icon-facebook.png" alt="" />
            <img src="/images/footer/icon-insta.png" alt="" />
          </div>
        </div>
      </div>
      <div className="coppy--right w-max">
        © 2020 BESTAYVN. ALL RIGHTS RESERVED.
      </div>
    </style.Container>
  )
}

export default Index
