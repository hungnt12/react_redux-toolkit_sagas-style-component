import Footer from "components/templates/Footer"
import Header from "components/templates/Header"
import Head from "next/head"
import React from "react"
import { usePage } from "../../hooks"

type Props = {
  children: React.ReactNode
  className?: string
}

export const Layout = function ({ children }: Props) {
  const { selectedPage } = usePage()

  return (
    <>
      <Head>
        <title>{selectedPage.title}</title>
      </Head>
      <Header />
      <article>{children}</article>
      <Footer />
    </>
  )
}
