import styles from "components/CarouselUltils/style"
import React from "react"
import Slider from "react-slick"

function SampleNextArrow(props: any) {
  const { addClassName, className, onClick } = props
  return (
    <styles.BtnCustom
      className={`${className} ${addClassName}`}
      onClick={onClick}
    />
  )
}

function SamplePrevArrow(props: any) {
  const { addClassName, className, onClick } = props
  return (
    <styles.BtnCustom
      className={`${className} ${addClassName}`}
      onClick={onClick}
    />
  )
}

const CustomArrows = ({ children, ...custom }: any) => {
  const settings = {
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: (
      <SampleNextArrow addClassName={`arrow-custom arrow-custom-next `} />
    ),
    prevArrow: (
      <SamplePrevArrow addClassName={`arrow-custom arrow-custom-prev `} />
    ),
    ...custom
  }
  return (
    <styles.ContainerCarousel style={{ width: "100%" }}>
      <Slider {...settings} >
        {children}
      </Slider>
    </styles.ContainerCarousel>
  )
}

export default CustomArrows
