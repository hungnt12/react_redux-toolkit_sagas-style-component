import styled from "styled-components"

const ContainerCarousel = styled.div``
const BtnCustom = styled.div`
  width: 48px;
  height: 48px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fe5202;
  border-radius: 999px;
  z-index: 2;
  // arrow
  &:hover {
    background-color: #fe5202;
  }
  &.arrow-custom-next {
    &:before {
      transform: rotate(180deg);
    }
  }
  &:before {
    content: url(/images/carousel/arrow-active.svg);
  }

  &.slick-disabled {
    background-color: white;
    border: solid 1px #fe5202;
    &:before {
      content: url(/images/carousel/arrow-disable.svg);
    }
  }
`
export default { BtnCustom, ContainerCarousel }
