import React from "react"
import styles from "./style"
function Index() {
  return (
    <styles.Container className="flex flex-wrap justify-between container">
      <div className="content_left flex flex-wrap ">
        <div className="left_content__top flex flex-wrap mb-6 w-full justify-between">
          <div className="content__left_item left_content__top__item__left">
            <a className="title z-10">Nhà riêng</a>
          </div>
          <div className="content__left_item left_content__top__item__right">
            <a className="title z-10">Nhà riêng</a>
          </div>
        </div>
        <div className="left_content__bottom flex flex-wrap w-full justify-between">
          <div className="content__left_item left_content__top__item__left">
            <a className="title z-10">Nhà riêng</a>
          </div>
          <div className="content__left_item left_content__top__item__right">
            <a className="title z-10">Nhà riêng</a>
          </div>
        </div>
      </div>
      <div className="content_right ">
        <div className="content_right_item w-full h-full rounded-xl bg-gray-500 ">
          <a className="title z-10">Nhà riêng</a>
        </div>
      </div>
    </styles.Container>
  )
}

export default Index
