import styled from "styled-components"

const Container = styled.div`
  .content__left_item {
    border-radius: 8px;
    height: 219.33px;
    background-color: red;
    max-width: 100%;
    position: relative;
    .title {
      position: absolute;
      bottom: 11.5px;
      left: 16px;
      font-weight: bold;
      color: white;
      line-height: 2.88rem;
    }
  }

  .content_left {
    width: ${(750 / 1170) * 100}%;
    .left_content__top {
      .left_content__top__item__left {
        width: 467.52px;
      }
      .left_content__top__item__right {
        width: 270px;
      }
    }
    .left_content__bottom {
      .left_content__top__item__left {
        width: 367.5px;
      }
      .left_content__top__item__right {
        width: 370px;
      }
    }
  }
  .content_right {
    width: ${(370 / 1170) * 100}%;
    max-width: 98%;
    margin: 0 auto;
    .content_right_item {
      position: relative;
      .title {
        position: absolute;
        bottom: 18px;
        left: 23.2px;
        font-weight: bold;
        color: white;
        line-height: 2.88rem;
      }
    }
  }
`
export default { Container }
