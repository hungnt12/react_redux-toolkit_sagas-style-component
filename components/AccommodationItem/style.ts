import styled from "styled-components";

const Item = styled.div`
  position: relative;
  width: 270px;
  padding-top: 170px;
  /* height: 318px; */
  margin: 0 auto;
  z-index: 1;
  .img_accommodation {
    position: absolute;
    top: 0;
    left: 0;
    background-color: red;
    width: 280px;
    height: 170px;
  }
  .content {
    margin-top: 23px;
    .address {
      line-height: 1.4rem;
      font-size: 1.2rem;
    }
    .title {
      margin-top: 12px;
      font-size: 1.5rem;
      line-height: 1.92rem;
    }
    .price {
      margin-top: 2rem;
      font-size: 1.4rem;
    }
    .evaluate {
      margin-top: 16px;
      color: #333333;
      font-size: 1.4rem;
    }
  }
`;

export default { Item };
