import React from "react"
import styles from "./style"
function Index() {
  return (
    <styles.Item className="flex flex-col">
      <div className="img_accommodation">
        {/* <ImageUltils src="/images/carousel/img-demo.svg" /> */}
      </div>
      <div className="content">
        <div className="address font-bold truncate">
          CĂN HỘ - QUẬN BA ĐÌNH
        </div>
        <div className="title font-bold line-clamp-2">
          The Autumn Homestay - Thiết kế độc đáo - View nhìn ra hồ Trúc Bạch
          The Autumn Homestay - Thiết kế độc đáo - View nhìn ra hồ Trúc Bạch

        </div>
        <div className="price">
          600.000đ/đêm
        </div>
        <div className="evaluate flex">
          <div className="flex mr-2">
            <img src="/images/item/starActive.svg" />
            <img src="/images/item/starActive.svg" />
            <img src="/images/item/starActive.svg" />
            <img src="/images/item/starActive.svg" />
            <img src="/images/item/starActive.svg" />
          </div> 3 đánh giá
        </div>
      </div>
    </styles.Item>
  )
}

export default Index
