import Image from "next/image"
import React from "react"

export default function ImageUltils({
  url,
  layout = "fill",
  alt = "",
  ...custom
}: any) {
  return (
    <>
      <Image src={url} alt={alt} layout={layout} {...custom} />{" "}
    </>
  )
}
