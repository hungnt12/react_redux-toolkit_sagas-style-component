import AccommodationItem from "components/AccommodationItem"
import AccommodationTypeItem from "components/AccommodationTypeItem"
import GalleryMasonry from "components/GalleryMasonry"
import DownLoadApp from "container/HomePage/DownLoadApp"
import Fillter from "container/HomePage/Fillter"
import Style from "container/HomePage/style"
import dynamic from "next/dynamic"
import React from "react"
const CarouselUltils = dynamic(() => import("components/CarouselUltils"))

function Index() {
  return (
    <Style.Container className="container mx-auto flex flex-wrap justify-between">
      <section className="home_section_1 w-full">
        <Fillter />
      </section>
      <section className="home_section_2 w-full">
        <div className="section_title">
          <h3>Loại căn hộ</h3>
        </div>
        <CarouselUltils>
          <AccommodationTypeItem />
          <AccommodationTypeItem />
          <AccommodationTypeItem />
          <AccommodationTypeItem />
          <AccommodationTypeItem />
        </CarouselUltils>
      </section>
      <section className="home_section_3 w-full">
        <div className="section_title">
          <h3>Điểm đến hấp dẫn</h3>
        </div>
        <GalleryMasonry />
      </section>
      <section className="home_section_4 w-full">
        <div className="section_title">
          <h3>Loại căn hộ</h3>
        </div>
        <CarouselUltils slidesToShow={4}>
          <AccommodationItem />
          <AccommodationItem />
          <AccommodationItem />
          <AccommodationItem />
          <AccommodationItem />
        </CarouselUltils>
      </section>
      <section className="home_section_5 w-full">
        <DownLoadApp />
      </section>
    </Style.Container>
  )
}

export default Index
