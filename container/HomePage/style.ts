import styled from "styled-components"

const Container = styled.div`
  .home_section_1 {
    .section_title {
      font-size: 2.4rem;
      font-weight: bold;
    }
  }

  .home_section_2 {
    margin-top: 67.8px;
  }
`
export default { Container }
