import { Select } from 'antd';
import React, { useState } from "react";
import DatePicker from "react-datepicker";
import NumberFormat from 'react-number-format';


const { Option } = Select;

function Index() {
    const [startDate, setStartDate] = useState<any>(new Date());
    const [endDate, setEndDate] = useState<any>(new Date());

    function onChange(value: any) {
        console.log(`selected ${value}`);
    }

    function onBlur() {
        console.log('blur');
    }

    function onFocus() {
        console.log('focus');
    }

    function onSearch(val: any) {
        console.log('search:', val);
    }
    return <div className="flex flex-wrap">
        <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Select a person"
            optionFilterProp="children"
            onChange={onChange}
            onFocus={onFocus}
            onBlur={onBlur}
            onSearch={onSearch}
            filterOption={(input: any, option: any) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
        >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="tom">Tom</Option>
        </Select>
        <div className="containet_select_date flex ">
            <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
            <DatePicker selected={endDate} onChange={(date: any) => setStartDate(date)} />
        </div>
        <div className="guaes_number">
            <NumberFormat value={1111111} format="####" />

        </div>
    </div>
}

export default Index
