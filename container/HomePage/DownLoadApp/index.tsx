import ImageUltils from 'components/ImageUltils'
import React from 'react'
import styles from './style'
function Index() {
    return (
        <styles.Container className="flex flex-wrap">
            <div className="img_download">
                <ImageUltils src="/images/homePage/bgDownLoad.svg" alt="" />
            </div>
            <div className="content_download">
                <div className="content">
                    <h6>Tải BESTAY trên thiết bị của bạn ngay!</h6>
                    <p>Những trải nghiệm du lịch đầy ý nghĩa cùng với Bestay.
                        Khám phá các điểm đến điểm đến và “nâng tầm” chuyến đi của
                        bạn bằng cách đặt nhà, căn hộ và phòng cho thuê trên nền tảng trực
                        tuyến tuyệt vời của chúng tôi.</p>
                </div>
                <div className="btn_box">
                    <button className='btn'><img src="/images/homePage/btn-google.svg" alt="" /></button>
                    <button className='btn'><img src="/images/homePage/btn-ios.svg" alt="" /></button>
                </div>
            </div>
        </styles.Container>
    )
}

export default Index
