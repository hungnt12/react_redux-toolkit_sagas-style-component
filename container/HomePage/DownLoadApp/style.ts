import styled from "styled-components"

const Container = styled.div`
    height: 400px;
    margin-top: 111.4px;
    .img_download{
      
        position: relative;
        margin-right: ${(40 / 1170) * 100}%;
        width: ${(430 / 1170) * 100}%;
        height: 100%;
    }
    .content_download{
        margin-top: 111.4px;
        width: ${(700 / 1170) * 100}%;
        word-break: break-word;
        .content{
            h6{
                color:#333333;
                font-size:2.4rem;
                margin-bottom: 16.3px;
            }
            p{
                
                font-size:1.6rem;
                line-height: 2.6rem;
            }
        }
        .btn_box{
            margin-top:35px;
            .btn{
                margin-right: 20.5px;
            }
        }
    }
`
export default { Container }
